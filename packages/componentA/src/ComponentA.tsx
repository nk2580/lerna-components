import React, { FunctionComponent } from 'react'
import {Button} from "@mui/material"

export const ComponentA: FunctionComponent = () => {
    return <Button >Tester Button</Button>
}